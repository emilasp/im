<?php

namespace tests\codeception\restapi\unit;

class TestCase extends \yii\codeception\TestCase
{
    public $appConfig = '@tests/codeception/config/restapi/unit.php';
}
