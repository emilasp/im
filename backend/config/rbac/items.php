<?php
use emilasp\user\core\rbac\PhpManager;

return [
    PhpManager::ROLE_GUEST => [
        'type' => 1,
        'description' => 'Гость',
        'ruleName' => 'userRole',
    ],
    PhpManager::ROLE_USER => [
        'type' => 1,
        'description' => 'Пользователь',
        'ruleName' => 'userRole',
        'children' => [
            PhpManager::ROLE_GUEST,
        ],
    ],
    'manager' => [
        'type' => 1,
        'description' => 'Менеджер',
        'ruleName' => 'userRole',
        'children' => [
            PhpManager::ROLE_GUEST,
            PhpManager::ROLE_USER,
        ],
    ],
    PhpManager::ROLE_ADMIN => [
        'type' => 1,
        'name' => 'admin',
        'description' => 'Админ',
        'ruleName' => 'userRole',
        'children' => [
            PhpManager::ROLE_GUEST,
            PhpManager::ROLE_USER,
            'manager',
        ],
    ],
];
