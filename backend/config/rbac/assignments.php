<?php
$session = Yii::$app->session;
$assignment = $session->get('assignment');

return \yii\helpers\Json::decode($assignment);
