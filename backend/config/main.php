<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'app-backend',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'backend\controllers',
    'defaultRoute'        => '/site/site/index/',
    'language'            => 'ru_RU',
    'sourceLanguage'      => 'en_US',
    'modules'             => [
        'core'       => [
            'class'       => 'emilasp\core\CoreModule',
            'enableTheme' => true,
            'theme'       => 'default',
            'isBackend'   => false,
        ],
        'variety'    => [
            'class' => 'emilasp\variety\VarietyModule',
        ],
        'json'       => [
            'class' => 'emilasp\json\JsonModule',
        ],
        'taxonomy'   => [
            'class' => 'emilasp\taxonomy\TaxonomyModule',
        ],
        'site'       => [
            'class' => 'emilasp\site\backend\SiteModule',
            'menu'  => include(__DIR__ . "/menu/menu.php"),
        ],
        'seo'        => [
            'class'       => 'emilasp\seo\backend\SeoModule',
            'pathModules' => [
                'geoapp' => '@vendor/emilasp/yii2-geoapp',
                'site'   => '@vendor/emilasp/yii2-site/frontend',
                'im'     => '@vendor/emilasp/yii2-im/frontend',
            ],
        ],
        'settings'   => [
            'class'       => 'emilasp\settings\SettingModule',
            'onAdminPage' => [
                \emilasp\site\backend\SiteModule::className(),
                \emilasp\seo\backend\SeoModule::className(),
                \emilasp\im\backend\ImModule::className(),
                \emilasp\variety\VarietyModule::className(),
            ],
        ],
        'user'       => [
            'class'                   => 'emilasp\user\core\UserModule',
            'duration'                => 2592000,
            'minLengthPassword'       => 5,
            'enterAfterRegistration'  => true,
            'socialEnabled'           => false,
            'registrationEnabled'     => false,
            'recoveryPasswordEnabled' => false,
            'loginLayout'             => '/login',
            'denyCallback'            => function ($rule, $action) {
                Yii::$app->controller->redirect('/user/login');
            },
        ],
        'user_admin' => [
            'class' => 'emilasp\user\backend\UserModule',
        ],
        'im'         => [
            'class' => 'emilasp\im\backend\ImModule',
        ],
        'gridview'   => [
            'class' => '\kartik\grid\Module',
        ],
    ],
    'components'          => [
        'request' => [
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
            'cookieValidationKey' => 'dfgdg343dfg.dfag2$dsfh34okl',
        ],
        'user'        => [
            'identityClass'   => 'emilasp\user\core\models\identity\Identity',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class'          => 'emilasp\user\core\rbac\PhpManager',
            'defaultRoles'   => [emilasp\user\core\rbac\PhpManager::ROLE_GUEST],
            'itemFile'       => '@frontend/config/rbac/items.php',
            'assignmentFile' => '@frontend/config/rbac/assignments.php',
            'ruleFile'       => '@frontend/config/rbac/rules.php',
        ],
        'urlManager'  => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>/<title:.*?>.html'  => '<_m>/<_c>/view',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>.html'              => '<_m>/<_c>/view',
                '<_m:[\w\-]+>/<_c:[\w\-]+>.html'                       => '<_m>/<_c>/index',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>.html' => '<_m>/<_c>/<_a>',/**/
            ],
        ],
        'view'        => [
            'class'           => '\rmrevin\yii\minify\View',
            'enableMinify'    => false,
            'web_path'        => '@web', // path alias to web base
            'base_path'       => '@webroot', // path alias to web base
            'minify_path'     => '@webroot/minify', // path alias to save minify result
            'js_position'     => [\yii\web\View::POS_END], // positions of js files to be minified
            'force_charset'   => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
            'expand_imports'  => true, // whether to change @import on content
            'compress_output' => false, // compress result html page
            'theme'           => [
                'pathMap' => [
                    '@app/views' => '@app/themes/default/views',
                ],
            ],
        ],

        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'levels'  => ['error', 'warning'],
                    'logFile' => '@app/runtime/logs/error.back.log',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/site/error',
        ],
    ],
    'params'              => $params,
];
