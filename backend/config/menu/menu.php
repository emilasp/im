<?php

return [
    [
        'label'  => '<i class="fa fa-money"></i> ' . Yii::t('im', 'Im'),
        'url'    => '#',
        'active' => false,
        'items'  => [
            [
                'label' => '<i class="fa fa-bars"></i> ' . Yii::t('im', 'Products'),
                'url'   => '#',
                'role'  => ['admin'],
                'items'  => [
                    ['label' => 'Список', 'url' => '/im/product/', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/im/product/create', 'role' => 'admin'],
                ],
            ],
            [
                'label' => '<i class="fa fa-database"></i> ' . Yii::t('im', 'Zakazy'),
                'url'   => '#',
                'role'  => ['admin'],
                'items'  => [
                    ['label' => 'Список', 'url' => '/im/file/', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/im/file/create', 'role' => 'admin'],
                ],
            ],
        ],
    ],
    [
        'label'  => '<i class="fa fa-fire"></i> ' . Yii::t('site', 'Adminimize'),
        'url'    => '#',
        'active' => false,
        'items'  => [
            [
                'label' => '<i class="fa fa-gears"></i> ' . Yii::t('settings', 'Settings'),
                'url'   => '/settings',
                'role'  => ['admin'],
            ],
            [
                'label' => 'Скрипты',
                'url'   => '#',
                'items' => [
                    [
                        'label' => 'Заполнение департаментов у продаж',
                        'url'   => '/adminimize/agents/fill-departments-for-sales',
                        'role'  => ['admin', 'adminimize_agents_fill-departments-for-sales'],
                    ],
                ],
            ],
        ],
    ],
    [
        'label'  => '<i class="fa fa-edit"></i> ' . Yii::t('site', 'Content'),
        'url'    => '#',
        'active' => false,
        'items'  => [
            [
                'label' => '<i class="fa fa-map-o"></i> ' . Yii::t('site', 'Pages'),
                'url'   => '#',
                'role'  => ['admin'],
                'items'  => [
                    ['label' => 'Список', 'url' => '/site/page/', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/site/page/create', 'role' => 'admin'],
                ],
            ],
            [
                'label' => '<i class="fa fa-folder"></i> ' . Yii::t('files', 'Files'),
                'url'   => '#',
                'role'  => ['admin'],
                'items'  => [
                    ['label' => 'Список', 'url' => '/files/file/', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/files/file/create', 'role' => 'admin'],
                ],
            ],
        ],
    ],
    [
        'label'  => '<i class="fa fa-group"></i> ' . Yii::t('user', 'Users'),
        'url'    => '#',
        'active' => false,
        'items'  => [
            ['label' => 'Список', 'url' => '/user_admin/user/', 'role' => 'admin'],
            ['label' => 'Добавить', 'url' => '/user_admin/user/create', 'role' => 'admin'],
            ['label' => 'Профили', 'url' => '/user_admin/profile/', 'role' => 'admin'],
            ['label' => 'Сервисы', 'url' => '/user_admin/service/', 'role' => 'admin'],
        ],
    ],
    [
        'label'   => '<i class="fa fa-key"></i> ' . Yii::t('user', 'Login'),
        'url'     => '/user/login',
        'visible' => !Yii::$app || (bool)Yii::$app->user->isGuest,
    ],
    [
        'label'   => '<i class="fa fa-key"></i> '
                     . Yii::t('user', 'Logout')
                     . ((Yii::$app && !Yii::$app->user->isGuest) ? Yii::$app->user->identity->username : ''),
        'url'     => '/user/login/logout',
        'visible' => Yii::$app && !Yii::$app->user->isGuest,
    ],
    /*
     [
        'label'  => '<i class="fa fa-sitemap"></i> Агенты',
        'url'    => '#',
        'active' => false,
        'items'  => [
            ['label' => 'Список', 'url' => '/agents/agent/', 'role' => ['admin', 'agents_agent_index']],
            [
                'label' => 'Добавить',
                'url'   => '/agents/agent/create',
                'role'  => ['admin', 'agents_agent_create'],
            ],
        ],
      ],
      [
        'label'  => '<i class="fa fa-shopping-cart"></i> Продажи',
        'url'    => '#',
        'active' => false,
        'items'  => [
            ['label' => 'Список продаж', 'url' => '/sales/sale/', 'role' => ['admin', 'sales_sale_index']],
            [
                'label' => 'Создать продажу',
                'url'   => '/sales/sale/create',
                'role'  => ['admin', 'sales_sale_create'],
            ],
            [
                'label' => 'Предварительные продажи',
                'url'   => '/sales/sale/pre-sales',
                'role'  => ['admin', 'sales_sale_pre-sales'],
            ],
            ['label' => 'Отчёты', 'url' => '/sales/report/', 'role' => ['admin', 'sales_report_index']],
            [
                'label' => 'Услуги/товары',
                'url'   => '/sales/report/',
                'role'  => ['admin', 'goods_good_index'],
                'items' => [
                    [
                        'label' => 'Список',
                        'url'   => '/goods/good/index',
                        'role'  => ['admin', 'goods_good_index'],
                    ],
                    [
                        'label' => 'Создать',
                        'url'   => '/goods/good/create',
                        'role'  => ['admin', 'goods_good_create'],
                    ],
                    [
                        'label' => 'Настройки для тарифов',
                        'url'   => '/goods/goodrate/index',
                        'role'  => ['admin', 'goods_goodrate_index'],
                    ],
                ],
            ],
            [
                'label' => 'Создать отчёт',
                'url'   => '/sales/report/create',
                'role'  => ['admin', 'sales_report_create'],
            ],
            [
                'label' => 'Логи sync',
                'url'   => '/sales/sync-sale/index',
                'role'  => ['admin', 'sales_syncsale_index'],
            ],
            [
                'label' => 'Расторжение',
                'url'   => '/sales/revoked/index',
                'role'  => ['admin', 'sales_revoked_index'],
            ],
            [
                'label' => 'Пользовательский экспорт',
                'url'   => '/sales/userexport/index',
                'role'  => ['admin', 'sales_userexport_index'],
                'rule'  => function () {
                    if (Yii::$app->user->isGuest) {
                        return false;
                    }
                    return Yii::$app->user->identity->role == 'admin';
                },
            ],
            [
                'label' => 'Продажи по дням',
                'url'   => '/sales/sale/sales-by-days',
                'role'  => ['admin', 'sales_sale_sales-by-days'],
            ],
        ],
    ],*/
];
