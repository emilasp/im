<?php

return [
    '<h3>' . Yii::t('im', 'GENERAL') . '</h3>',
    [
        'label'  => '<i class="fa fa-money"></i> ' . Yii::t('im', 'Im'),
        'url'    => '#',
        'active' => false,
        'items'  => [
            [
                'label'  => Yii::t('im', 'Products'),
                'url'    => '#',
                'active' => true,
                'icon'   => 'fa fa-bars',
                'role'   => ['admin', 'manager'],
                'items'  => [
                    ['label' => 'Список', 'url' => '/im/product/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/im/product/create', 'role' => 'admin'],
                ],
            ],
            [
                'label' => Yii::t('im', 'Orders'),
                'url'   => '#',
                'icon'  => 'fa fa-database',
                'role'  => ['admin', 'manager'],
                'items' => [
                    ['label' => 'Список', 'url' => '/im/order/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/im/order/create', 'role' => 'admin'],
                ],
            ],
            [
                'label' => Yii::t('im', 'Sellers'),
                'url'   => '#',
                'icon'  => 'fa fa-database',
                'role'  => ['admin', 'manager'],
                'items' => [
                    ['label' => 'Список', 'url' => '/im/seller/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/im/seller/create', 'role' => 'admin'],
                ],
            ],
            [
                'label' => Yii::t('im', 'Brands'),
                'url'   => '#',
                'icon'  => 'fa fa-database',
                'role'  => ['admin', 'manager'],
                'items' => [
                    ['label' => 'Список', 'url' => '/im/brand/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/im/brand/create', 'role' => 'admin'],
                ],
            ],
            [
                'label' => Yii::t('im', 'Order statuses'),
                'url'   => '#',
                'icon'  => 'fa fa-database',
                'role'  => ['admin', 'manager'],
                'items' => [
                    ['label' => 'Список', 'url' => '/im/order-status/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/im/order-status/create', 'role' => 'admin'],
                ],
            ],
            [
                'label' => Yii::t('im', 'Clients'),
                'url'   => '#',
                'icon'  => 'fa fa-database',
                'role'  => ['admin', 'manager'],
                'items' => [
                    ['label' => 'Список', 'url' => '/im/client/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/im/client/create', 'role' => 'admin'],
                ],
            ],
            [
                'label' => Yii::t('im', 'Deliveries'),
                'url'   => '#',
                'icon'  => 'fa fa-database',
                'role'  => ['admin', 'manager'],
                'items' => [
                    ['label' => 'Список', 'url' => '/im/delivery/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/im/delivery/create', 'role' => 'admin'],
                ],
            ],
            [
                'label' => Yii::t('im', 'Payments methods'),
                'url'   => '#',
                'icon'  => 'fa fa-database',
                'role'  => ['admin', 'manager'],
                'items' => [
                    ['label' => 'Список', 'url' => '/im/payment/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/im/payment/create', 'role' => 'admin'],
                ],
            ],
            [
                'label' => Yii::t('im', 'Import products'),
                'url'   => '#',
                'icon'  => 'fa fa-database',
                'role'  => ['admin', 'manager'],
                'items' => [
                    [
                        'label' => Yii::t('im', 'Import'),
                        'url'   => '/im/import/import-file',
                        'role'  => 'admin',
                        'manager'
                    ],
                    [
                        'label' => Yii::t('im', 'Import history'),
                        'url'   => '/im/file/import-history',
                        'role'  => 'admin',
                        'manager'
                    ],
                ],
            ],
        ],
    ],
    '<h3>' . Yii::t('site', 'Site') . '</h3>',
    [
        'label'  => Yii::t('site', 'Content'),
        'url'    => '#',
        'icon'   => 'fa fa-edit',
        'active' => false,
        'items'  => [
            [
                'label' => Yii::t('site', 'Pages'),
                'url'   => '#',
                'icon'  => 'fa fa-map-o',
                'role'  => ['admin'],
                'items' => [
                    ['label' => 'Список', 'url' => '/site/page/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/site/page/create', 'role' => 'admin'],
                ],
            ],
            [
                'label' => Yii::t('files', 'Files'),
                'url'   => '#',
                'icon'  => 'fa fa-folder',
                'role'  => ['admin'],
                'items' => [
                    ['label' => 'Список', 'url' => '/files/file/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/files/file/create', 'role' => 'admin'],
                ],
            ],
        ],
    ],
    [
        'label'  => Yii::t('seo', 'Seo'),
        'url'    => '#',
        'icon'   => 'fa fa-edit',
        'active' => false,
        'items'  => [
            [
                'label' => Yii::t('seo', 'Seo Links'),
                'url'   => '#',
                'icon'  => 'fa fa-map-o',
                'role'  => ['admin'],
                'items' => [
                    ['label' => 'Список', 'url' => '/seo/seo/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/seo/seo/create', 'role' => 'admin'],
                    ['label' => 'Сканер', 'url' => '/seo/seo/scanner', 'role' => 'admin'],
                ],
            ],
        ],
    ],
    '<h3>' . Yii::t('im', 'Tt') . '</h3>',
    [
        'label'  => Yii::t('site', 'Adminimize'),
        'url'    => '#',
        'icon'   => 'fa fa-wrench',
        'active' => false,
        'items'  => [
            [
                'label' => Yii::t('settings', 'Settings'),
                'url'   => '/settings',
                'icon'  => 'fa fa-gears',
                'role'  => ['admin'],
            ],
            [
                'label' => Yii::t('variety', 'Varieties'),
                'url'   => '/variety/variety/index',
                'icon'  => 'fa fa-gears',
                'role'  => ['admin'],
                'items' => [
                    ['label' => 'Список', 'url' => '/variety/variety/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/variety/variety/create', 'role' => 'admin'],
                ],
            ],
            [
                'label' => Yii::t('json', 'Schemes'),
                'url'   => '/json/json-scheme/index',
                'icon'  => 'fa fa-gears',
                'role'  => ['admin'],
                'items' => [
                    ['label' => 'Список', 'url' => '/json/json-scheme/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/json/json-scheme/create', 'role' => 'admin'],
                ],
            ],
        ],
    ],
    '<h3>' . Yii::t('taxonomy', 'Taxonomy') . '</h3>',
    [
        'label'  => Yii::t('taxonomy', 'Taxonomy'),
        'url'    => '#',
        'icon'   => 'fa fa-share-alt',
        'active' => false,
        'items'  => [
            [
                'label' => Yii::t('taxonomy', 'Categories'),
                'url'   => '/taxonomy/category-type/index',
                'icon'  => 'fa fa-tree',
                'role'  => ['admin'],
                'items' => [
                    ['label' => 'Список', 'url' => '/taxonomy/category-type/index', 'role' => 'admin'],
                    ['label' => 'Добавить', 'url' => '/taxonomy/category-type/create', 'role' => 'admin'],
                ],
            ],
            [
                'label' => Yii::t('taxonomy', 'Properties'),
                'url'   => '/taxonomy/property/index',
                'icon'  => 'fa fa-circle',
                'role'  => ['admin'],
                'items' => [
                    [
                        'label' => Yii::t('taxonomy', 'Properties'),
                        'url'   => '/taxonomy/property/index',
                        'role'  => 'admin'
                    ],
                    [
                        'label' => Yii::t('taxonomy', 'Add property'),
                        'url'   => '/taxonomy/property/create',
                        'role'  => 'admin'
                    ],
                    '<hr />',
                    [
                        'label' => Yii::t('taxonomy', 'Groups'),
                        'url'   => '/taxonomy/property-group',
                        'role'  => 'admin'
                    ],
                    [
                        'label' => Yii::t('taxonomy', 'Groups create'),
                        'url'   => '/taxonomy/property-group/create',
                        'role'  => 'admin'
                    ],
                    '<hr />',
                    [
                        'label' => Yii::t('taxonomy', 'Values'),
                        'url'   => '/taxonomy/property-value',
                        'role'  => 'admin'
                    ],
                    [
                        'label' => Yii::t('taxonomy', 'Value create'),
                        'url'   => '/taxonomy/property-value/create',
                        'role'  => 'admin'
                    ],
                ],
            ],
        ],
    ],

    [
        'label'  => Yii::t('user', 'Users'),
        'url'    => '#',
        'icon'   => 'fa fa-group',
        'active' => false,
        'items'  => [
            ['label' => 'Список', 'url' => '/user_admin/user/index', 'role' => 'admin'],
            ['label' => 'Добавить', 'url' => '/user_admin/user/create', 'role' => 'admin'],
            ['label' => 'Профили', 'url' => '/user_admin/profile/index', 'role' => 'admin'],
            ['label' => 'Сервисы', 'url' => '/user_admin/service/index', 'role' => 'admin'],
        ],
    ],
    [
        'label'   => Yii::t('user', 'Login'),
        'url'     => '/user/login',
        'icon'    => 'fa fa-key',
        'visible' => !Yii::$app || (bool)Yii::$app->user->isGuest,
    ],
    [
        'label'   => Yii::t('user', 'Logout') . ' <small>(' . Yii::$app->user->identity->username . ')</small>',
        'url'     => '/user/login/logout',
        'icon'    => 'fa fa-key',
        'visible' => Yii::$app && !Yii::$app->user->isGuest,
    ],
];
