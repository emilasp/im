<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use emilasp\admintheme\bundles\ThemeAsset;
use emilasp\site\backend\assets\AppAsset;
use emilasp\site\common\extensions\menu\Menu;
use emilasp\admintheme\widgets\menu\MenuAdmin;
use emilasp\admintheme\widgets\userMenuAdmin\UserMenuAdmin;
use emilasp\admintheme\widgets\skinSwitcher\SkinSwitcherAdmin;
?>

<?php AppAsset::register($this); ?>
<?php ThemeAsset::register($this); ?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="signin">
    <div id="wrapper">

        <a href="index.html" class="logo">
            <i class="brankic-pen"></i>
        </a>

        <h3>Welcome back!</h3>

        <div class="content">
            <?= $content ?>
        </div>

        <div class="bottom-wrapper">
            <div class="message">
                <span>Don't have an account?</span>
                <a href="signup.html">Sign up here</a>.
            </div>
        </div>

    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
