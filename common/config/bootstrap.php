<?php
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('restapi', dirname(dirname(__DIR__)) . '/restapi');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
