<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,

    'media'=>[
        'path'=>'@app/web/media',
        'url'=>'@web/media',
        'pathUploadEditor'=>'@app/web/upload/images',
        'urlUploadEditor'=>'@web/upload/images',
        'defaultExtForImage'=>'jpg',
        'waterMark'=>'@backend/system/watermark.png',
        'scaleWaterMark'=>5,                                        // 1/5 от изображения
        'defaultSize'=>['width'=>600,'height'=>400],                // используется при аплоаде Editor
        'quality'=>100,

        'defaultImageFolder'=>'@app/web/system/media/default',

        'types'=>[
            'default'=>[
                'watermark'=>true,
                'sizes'=>[
                    'ico'=>[ 'type'=>'crop',  'size'=>['width'=>20,'height'=>20], 'default'=>'tag20x20.png' ],
                    'min'=>[ 'type'=>'crop',  'size'=>['width'=>90,'height'=>70], 'default'=>'tag.png' ],
                    'mid'=>[ 'type'=>'crop',  'size'=>['width'=>200,'height'=>200], 'default'=>'tag.png' ],
                    'med'=>[ 'type'=>'ratio', 'size'=>['width'=>300,'height'=>1000], 'default'=>'tag.png' ],
                    'max'=>[ 'type'=>'ratio', 'size'=>['width'=>800,'height'=>4000], 'default'=>'tag.png' ],
                ],
            ],

            'profile'=>[
                'watermark'=>false,
                'defaultImage'=>'/system/icons/users/min_avatar.jpg',
                'sizes'=>[
                    'min'=>[ 'type'=>'crop',  'size'=>['width'=>50,'height'=>50], 'default'=>'noavatar50x50.png' ],
                    'med'=>[ 'type'=>'ratio', 'size'=>['width'=>100,'height'=>2000], 'default'=>'noavatar100x100.png' ],
                    'max'=>[ 'type'=>'ratio', 'size'=>['width'=>200,'height'=>4000], 'default'=>'noavatar200x200.png' ],
                ]
            ]
        ],
    ],

];
