<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'app-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log', 'imports'],
    'controllerNamespace' => 'console\controllers',
    'components'          => [
        'log'   => [
            'targets' => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'controllerMap'       => [
        'websocket' => 'emilasp\websocket\console\controllers\WebsocketController',
        'migrate' => [
            'class'   => 'emilasp\core\commands\MigrateController',
            'default' => [
                'db'     => 'db',
                'module' => 'app',
            ],
            'modules' => [
                'user'     => '@vendor/emilasp/yii2-user/migrations',
                'variety'  => '@vendor/emilasp/yii2-variety/migrations',
                'settings' => '@vendor/emilasp/yii2-settings/migrations',
                'json'     => '@vendor/emilasp/yii2-json/migrations',
                'geo'      => '@vendor/emilasp/yii2-geoapp/migrations',
                'seo'      => '@vendor/emilasp/yii2-seo/migrations',
                'files'    => '@vendor/emilasp/yii2-files/migrations',
                'taxonomy' => '@vendor/emilasp/yii2-taxonomy/migrations',
                'site'     => '@vendor/emilasp/yii2-site/migrations',
                'im'       => '@vendor/emilasp/yii2-im/migrations',
                'goal'     => '@vendor/emilasp/yii2-goal/migrations',
                'app'      => '@app/migrations',
            ],
        ],
    ],
    'params'              => $params,
];
