<?php
return [
    0 => [
        'type' => 1,
        'description' => 'Гость',
        'ruleName' => 'userRole',
    ],
    1 => [
        'type' => 1,
        'description' => 'Пользователь',
        'ruleName' => 'userRole',
        'children' => [
            0,
        ],
    ],
    2 => [
        'type' => 1,
        'description' => 'Мастер',
        'ruleName' => 'userRole',
        'children' => [
            0,
            1,
        ],
    ],
    3 => [
        'type' => 1,
        'description' => 'Компания',
        'ruleName' => 'userRole',
        'children' => [
            0,
            1,
            2,
        ],
    ],
    4 => [
        'type' => 1,
        'description' => 'Админ',
        'ruleName' => 'userRole',
        'children' => [
            0,
            1,
            2,
            3,
        ],
    ],
    'admin' => [
        'type' => 1,
    ],
];
