<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'app-frontend',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute'        => '/site/site',
    'language'            => 'ru_RU',
    'sourceLanguage'      => 'en_US',
    'modules'             => [
        'gridview'   => [
            'class' => '\kartik\grid\Module',
        ],
        'geoapp' => [
            'class'        => 'emilasp\geoapp\GeoModule',
            'countryAllow' => [],
        ],
        'core'   => [
            'class'             => 'emilasp\core\CoreModule',
            'enableTheme'       => true,
            'enableModuleTheme' => true,
            'theme'             => 'default',
        ],
        'site'   => [
            'class' => 'emilasp\site\frontend\SiteModule',
        ],
        'seo'    => [
            'class' => 'emilasp\seo\frontend\SeoModule',
        ],
        'user'   => [
            'class'                  => 'emilasp\user\core\UserModule',
            'duration'               => 2592000,
            'minLengthPassword'      => 5,
            'enterAfterRegistration' => true,
            'enableTheme'            => true,
            'routeAfterLogin'        => '/goal/goal-dashboard',
        ],
        'im'     => [
            'class'          => 'emilasp\im\frontend\ImModule',
            'categoryTypeId' => 279,
        ],
        'goal'   => [
            'class' => \emilasp\goal\frontend\GoalModule::className(),
        ],
    ],
    'components'          => [
        'request' => [
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
            'cookieValidationKey' => 'dfgdg343dfg.dfag2$dsfh34okl',
        ],
        'authClientCollection' => [
            'class'   => 'yii\authclient\Collection',
            'clients' => [
                'google'   => [
                    'class' => 'yii\authclient\clients\GoogleOpenId',
                ],
                'facebook' => [
                    'class'        => 'yii\authclient\clients\Facebook',
                    'clientId'     => '418058068400899',
                    'clientSecret' => '8925a935551f96bd60bc5819fbd3e37e',
                    'title'        => 'Facebook',
                ],
                'twitter'  => [
                    'class'          => '\emilasp\user\core\authclient\clients\Twitter',
                    'consumerKey'    => 'h9TwIAuw1F7FWRXRAurQLbCE0',
                    'consumerSecret' => '5HVAGk8dQUWXVpVF1EWy3M6nG3V8UWZPFyjxvKfHiA5lPj4TVa',
                ],
            ],
        ],
        'user'                 => [
            'identityClass'   => 'emilasp\user\core\models\identity\Identity',
            'enableAutoLogin' => true,
        ],
        'authManager'          => [
            'class'          => 'emilasp\user\core\rbac\PhpManager',
            'defaultRoles'   => [emilasp\user\core\rbac\PhpManager::ROLE_GUEST],
            'itemFile'       => '@frontend/config/rbac/items.php',
            'assignmentFile' => '@frontend/config/rbac/assignments.php',
            'ruleFile'       => '@frontend/config/rbac/rules.php',
        ],
        'access'               => [
            'class' => 'yii\web\AccessControl',
        ],
        'urlManager'           => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                '' => 'site/site/index',

                '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>/<title:.*?>.html' => '<_m>/<_c>/view',
                '<_m:[\w\-]+>/<_c:[\w\-]+>.html'                      => '<_m>/<_c>/index',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>.html'              => '<_m>/<_c>/<_a>',


                ['pattern' => 'sitemap', 'route' => '/site/sitemap/index', 'suffix' => '.xml'],

            ],

        ],
        'view'                 => [
            'class'           => 'emilasp\core\components\base\View',
            'enableMinify'    => !YII_DEBUG,
            'web_path'        => '@web', // path alias to web base
            'base_path'       => '@webroot', // path alias to web base
            'minify_path'     => '@webroot/minify', // path alias to save minify result
            'js_position'     => [\yii\web\View::POS_END], // positions of js files to be minified
            'force_charset'   => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
            'expand_imports'  => true, // whether to change @import on content
            'compress_output' => true, // compress result html page
            'theme'           => [
                'basePath' => '',
            ],
        ],

        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'levels'  => ['error', 'warning'],
                    'logFile' => '@app/runtime/logs/error.front.log',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/site/error',
        ],
    ],
    'params'              => $params,
];
