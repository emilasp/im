<?php
use yii\helpers\Url;
?>
<div class="navbar yamm navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            <a href="/" class="pull-left logo"><img src="/images/logo.png" height="40px"></a>
            <a href="/" class="navbar-brand"><?= Yii::t('goal', 'Goals') ?></a>
        </div>
        <div id="navbar-collapse-1" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <!-- Classic list -->
                <?php if (!Yii::$app->user->isGuest) : ?>
                <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle"
 aria-expanded="false"><?= Yii::t('goal', 'Goals') ?><b class="caret"></b></a>
                        <ul class="dropdown-menu full">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <a href="<?= Url::toRoute(['/goal/goal-dashboard']) ?>">
                                                        <?= Yii::t('goal', 'Goal dashboard') ?>
                                                    </a><br />
                                                    <a href="<?= Url::toRoute(['/goal/goal-dashboard/calendar']) ?>">
                                                        <?= Yii::t('goal', 'Goal calendar') ?>
                                                    </a><br />
                                                    <a href="<?= Url::toRoute(['/goal/goal-direction/create']) ?>">
                                                        <?= Yii::t('goal', 'Goal direction create') ?>
                                                    </a><br />
                                                    <a href="<?= Url::toRoute(['/goal/goal-project/create']) ?>">
                                                        <?= Yii::t('goal', 'Goal project create') ?>
                                                    </a><br />
                                                    <a href="<?= Url::toRoute(['/goal/goal/create']) ?>">
                                                        <?= Yii::t('goal', 'Goal create') ?>
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <a href="<?= Url::toRoute(['/goal/goal-direction']) ?>">
                                                        <?= Yii::t('goal', 'Goal directions') ?>
                                                    </a><br />
                                                    <a href="<?= Url::toRoute(['/goal/goal-project']) ?>">
                                                        <?= Yii::t('goal', 'Goal projects') ?>
                                                    </a><br />
                                                    <a href="<?= Url::toRoute(['/goal/goal']) ?>">
                                                        <?= Yii::t('goal', 'Goals') ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                <?php endif ?>

                <!-- Accordion demo -->
                <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Accordion<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div id="accordion" class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Collapsible Group Item #1</a></h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in">
                                                <div class="panel-body">Ut consectetur ullamcorper purus a rutrum. Etiam dui nisi, hendrerit feugiat scelerisque et, cursus eu magna. </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Collapsible Group Item #2</a></h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse">
                                                <div class="panel-body">Nullam pretium fermentum sapien ut convallis. Suspendisse vehicula, magna non tristique tincidunt, massa nisi luctus tellus, vel laoreet sem lectus ut nibh. </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Collapsible Group Item #3</a></h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse">
                                                <div class="panel-body">Praesent leo quam, faucibus at facilisis id, rhoncus sit amet metus. Sed vitae ipsum non nibh mattis congue eget id augue. </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Classic dropdown -->
                <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Classic<b class="caret"></b></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a tabindex="-1" href="#"> Action </a></li>
                        <li><a tabindex="-1" href="#"> Another action </a></li>
                        <li><a tabindex="-1" href="#"> Something else here </a></li>
                        <li class="divider"></li>
                        <li><a tabindex="-1" href="#"> Separated link </a></li>
                    </ul>
                </li>
                <!-- Pictures -->
                <li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Pictures<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-2"><a href="#" class="thumbnail"><img alt="150x190" src="demo/img/190.jpg"></a></div>
                                    <div class="col-xs-6 col-sm-2"><a href="#" class="thumbnail"><img alt="150x190" src="demo/img/190.jpg"></a></div>
                                    <div class="col-xs-6 col-sm-2"><a href="#" class="thumbnail"><img alt="150x190" src="demo/img/190.jpg"></a></div>
                                    <div class="col-xs-6 col-sm-2"><a href="#" class="thumbnail"><img alt="150x190" src="demo/img/190.jpg"></a></div>
                                    <div class="col-xs-6 col-sm-2"><a href="#" class="thumbnail"><img alt="150x190" src="demo/img/190.jpg"></a></div>
                                    <div class="col-xs-6 col-sm-2"><a href="#" class="thumbnail"><img alt="150x190" src="demo/img/190.jpg"></a></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>



            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (Yii::$app->user->isGuest) : ?>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle"
                           aria-expanded="false"><?= Yii::t('user', 'Login') ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="<?= Url::toRoute(['/user/login']) ?>">
                                                <?= Yii::t('user', 'Login') ?>
                                            </a><br />
                                            <a href="<?= Url::toRoute(['/user/registration']) ?>">
                                                <?= Yii::t('user', 'Registration') ?>
                                            </a>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                <?php else : ?>
                    <li class="color-white"><?= date('Y-m-d H:i:s') ?></li>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle"
                           aria-expanded="false"><?= Yii::$app->user->identity->username ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="<?= Url::toRoute(['/user/profile']) ?>">
                                                <?= Yii::t('user', 'Profile') ?>
                                            </a><br />
                                            <a href="<?= Url::toRoute(['/user/login/logout']) ?>">
                                                <?= Yii::t('user', 'Logout') ?>
                                            </a>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                <?php endif ?>
            </ul>
        </div>
    </div>
</div>