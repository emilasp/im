<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;

$this->params['breadcrumbs'][] = $model->name;
?>
<div class="page">
    <h1><?= Html::encode($model->name) ?></h1>

        <?= Html::decode($model->text) ?>
</div>
