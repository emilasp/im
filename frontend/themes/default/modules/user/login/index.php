<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use emilasp\user\core\widgets\authSocial\AuthSocial;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title                   = Yii::t('user', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login well">
    <h1><?= Html::encode($this->title) ?></h1>


    <?= AuthSocial::widget() ?>

    <p><?= Yii::t('user', 'Please fill out the following fields to login:') ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="row">
                <div class="col-md-6">
                    <?php if (Yii::$app->getModule('user')->recoveryPasswordEnabled) : ?>
                        <?= Html::a(Yii::t('user', 'Recovery password'), ['site/request-password-reset']) ?>
                    <?php endif ?>
                </div>
                <div class="col-md-6">
                    <?php if (Yii::$app->getModule('user')->registrationEnabled) : ?>
                        <?= Html::a(Yii::t('user', 'Registration'), ['site/request-password-reset']) ?>
                    <?php endif ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
