<?php

/* @var $this \yii\web\View */
/* @var $content string */

use emilasp\site\common\extensions\FlashMsg\FlashMsg;
use emilasp\site\widgets\megamenu\Megamenu;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use emilasp\site\frontend\assets\AppAsset;
use emilasp\site\common\extensions\skins\Skins;
use emilasp\websocket\common\widgets\ws\WsConsoleWidget;

?>

<?php AppAsset::register($this); ?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?= Skins::widget(['theme' => Skins::THEME_GOAL]) ?>

</head>
<body>
<?php $this->beginBody() ?>

<?= WsConsoleWidget::widget() ?>

<?php FlashMsg::widget(); ?>

<div id="wrapper" class="container">
    <header id="header">
        <?= Megamenu::widget() ?>
    </header>

    <div class="container breadcrunb-fixed-wrapper">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>

    <div class="bg"></div>

    <section id="main" class="row">




        <?php
        $mainColumns = 12;
        if (!empty($this->params['sidebar']['left']) && !empty($this->params['sidebar']['right'])) {
            $mainColumns = 6;
        } elseif (!empty($this->params['sidebar']['left']) || !empty($this->params['sidebar']['right'])) {
            $mainColumns = 9;
        }
        ?>

        <?php if (!empty($this->params['sidebar']['left'])) : ?>
            <div id="sidebar" class="col-md-3">
                <?= $this->params['sidebar']['left'] ?>
            </div>

        <?php endif ?>

        <div id="featured" class="col-md-<?= $mainColumns ?>">

            <?= $content ?>

        </div>

        <?php if (!empty($this->params['sidebar']['right'])) : ?>
            <div id="sidebar" class="col-md-3">
                <div class="well">
                    <?= $this->params['sidebar']['right'] ?>
                </div>
            </div>
        <?php endif ?>

    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="copyrights col-sm-6 col-md-6">
                    &copy; My Company <?= date('Y') ?>
                </div>
                <div class="social-icons col-sm-6 col-md-6">
                    <br>.social-icons
                </div>
            </div>
        </div>
    </footer>
</div>

<?php
$js = <<<JS
    $( document ).ready(function() {
        $(window).scroll(function(e){
          var scrolled = $(window).scrollTop();
          $('.bg').css('top',-(scrolled*0.2)+'px');
        });
    });
JS;
$this->registerJs($js);
?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

